% Copyright (c) 2020, AlaskanEmily
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module resistors.

%=============================================================================%
% A very naive resistor network calculator.
% I wrote this because I just wanted an answer to how to get as close to an
% ideal resistance as possible using only the resistor values I had on hand.
%=============================================================================%
:- interface.
%=============================================================================%

:- use_module io.

:- pred main(io.io::di, io.io::uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module char.
:- import_module int.
:- import_module float.
:- import_module list.
:- use_module maybe.
:- use_module rbtree.
:- use_module solutions.
:- use_module std_util.
:- use_module string.

%-----------------------------------------------------------------------------%

:- pred print(network::in, io.io::di, io.io::uo) is det.
print(Network) -->
    io.write_int(value(Network)), io.nl,
    print(0, Network), io.nl, io.nl.

%-----------------------------------------------------------------------------%

:- pred print_indent(int::in, io.io::di, io.io::uo) is det.
print_indent(N, !IO) :-
    ( N = 0 -> true ; io.write_string("  ", !IO), print_indent(N-1, !IO) ).

%-----------------------------------------------------------------------------%

:- pred print(int::in, network::in, io.io::di, io.io::uo) is det.

print(Indent, resistor(I)) -->
    print_indent(Indent),
    io.write_char(('r')),
    io.write_int(I),
    io.nl.

print(Indent, series(L)) -->
    print_indent(Indent),
    io.write_char('s'), io.write_int(list.length(L)), io.nl,
    list.foldl(print(Indent+1), L).

print(Indent, parallel(L)) -->
    print_indent(Indent),
    io.write_char('p'), io.write_int(list.length(L)), io.nl,
    list.foldl(print(Indent+1), L).

%-----------------------------------------------------------------------------%
:- func tolerance(int) = int.
% It's OK to get within 10%.
tolerance(A) = (A / 10).

%-----------------------------------------------------------------------------%
% close_enough(Target, Other)
:- pred close_enough(int::in, int::in) is semidet.
close_enough(A, B) :- abs(A - B) < tolerance(A).

%-----------------------------------------------------------------------------%

:- type network --->
    series(list(network)) ;
    parallel(list(network)) ;
    resistor(int).

%-----------------------------------------------------------------------------%

:- func safe_inverse(float) = float.
safe_inverse(F) = O :-
    ( F = 0.0 -> O = 0.0 ; O = 1.0 / F ).

:- func value(network) = int.
value(resistor(I)) = I.
value(series(L)) = list.foldl((func(N, I) = (I + value(N))), L, 0).
value(parallel(L)) = round_to_int(
    safe_inverse(list.foldl((func(N, F) = (F + safe_inverse(float(value(N))))),
        L, 0.0))).

%-----------------------------------------------------------------------------%

:- func reduce(network) = network.
reduce(N) = resistor(value(N)).

%-----------------------------------------------------------------------------%

:- func length(network) = int.
length(resistor(_)) = 1.
length(series(L)) = list.foldl((func(N, I) = (I + length(N))), L, 0).
length(parallel(L)) = list.foldl((func(N, I) = (I + length(N))), L, 0).

%-----------------------------------------------------------------------------%

:- type resistor_set == rbtree.rbtree(int, int).

%-----------------------------------------------------------------------------%

:- pred try_read_resistor(string::in, int::out, int::out) is semidet.

try_read_resistor(Line, Value, Count) :-
    % Hyper-lenient parser.
    Words = string.words_separator(std_util.isnt(char.is_digit),
        list.head(string.words_separator(builtin.unify('#'), Line))),
    Words = [ValueStr|[CountStr|_]],
    string.to_int(ValueStr, Value),
    string.to_int(CountStr, Count).

%-----------------------------------------------------------------------------%

:- pred read_resistors_from_stdin(resistor_set, resistor_set, io.io, io.io).
:- mode read_resistors_from_stdin(in, out, di, uo) is det.
read_resistors_from_stdin(!Tree, !IO) :-
    io.read_line_as_string(StringResult, !IO),
    ( if
        StringResult = io.ok(String)
    then
        Trimmed = string.strip(String),
        ( if
            string.is_empty(Trimmed)
        then
            read_resistors_from_stdin(!Tree, !IO)
        else if
            try_read_resistor(Trimmed, Value, Count)
        then
            rbtree.set(Value, Count, !Tree),
            read_resistors_from_stdin(!Tree, !IO)
        else
            true
        )
    else
        true
    ).

%-----------------------------------------------------------------------------%

:- func rec_depth = int.
rec_depth = 8.

%-----------------------------------------------------------------------------%
% simple_series(Target,
%    InputNetwork,
%    OutNetwork,
%    !Resistors)
:- pred simple_series(int, int,
    list.list(network),
    network,
    resistor_set, resistor_set).
:- mode simple_series(in, in, in, out, in, out) is nondet.

simple_series(MaxDepth,
    Target,
    InputList,
    OutNetwork,
    !Resistors) :-
    MaxDepth > 0,
    InputNetwork = series(InputList),
    NetworkResistance = value(InputNetwork),
    ( if
        % Resistance will only get higher with a simple series.
        % No need to test for empty networks.
        Target - tolerance(Target) > NetworkResistance
    then
        rbtree.member(!.Resistors, Value, Count),
        ( if
            Count =< 1
        then
            rbtree.delete(Value, !Resistors)
        else
            rbtree.set(Value, Count - 1, !Resistors)
        ),
        simple_series(MaxDepth - 1,
            Target,
            [resistor(Value)|InputList],
            OutNetwork,
            !Resistors)
    else
        require_semidet (
            % Test we didn't exceed the max.
            Target + tolerance(Target) >= NetworkResistance,
            % Solved.
            OutNetwork = InputNetwork
        )
    ).

%-----------------------------------------------------------------------------%
% simple_parallel(MaxDepth, Target,
%    InputNetwork, InputResistanceSum,
%    OutNetwork,
%    !Resistors)
:- pred simple_parallel(int, int,
    list.list(network),
    network,
    resistor_set, resistor_set).
:- mode simple_parallel(in, in, in, out, in, out) is nondet.

simple_parallel(MaxDepth,
    Target,
    InputList,
    OutNetwork,
    !Resistors) :-
    MaxDepth > 0,
    InputNetwork = parallel(InputList),
    NetworkResistance = value(InputNetwork),
    ( if
        % The resistance will only get lower for a parallel network, IF there is
        % already something in the network.
        InputList = [] ;
        Target + tolerance(Target) < NetworkResistance
    then
        % Add more to the existing network.
        rbtree.member(!.Resistors, Value, Count),
        ( if
            Count =< 1
        then
            rbtree.delete(Value, !Resistors)
        else
            rbtree.set(Value, Count - 1, !Resistors)
        ),
        simple_parallel(MaxDepth - 1,
            Target,
            [resistor(Value)|InputList],
            OutNetwork, !Resistors)
    else
        require_semidet (
            % Test we didn't get below the minimum.
            Target - tolerance(Target) < NetworkResistance,
            % Solved.
            OutNetwork = InputNetwork
        )
    ).

%-----------------------------------------------------------------------------%
% simple(Target, Result, !Resistors
:- pred simple(int, network, resistor_set, resistor_set).
:- mode simple(in, out, in, out) is nondet.

% HYPER simplistic. Will only create single-level networks.
% This will need more work!
simple(Target, Result, !Resistors) :-
    % See if there are any values which are close enough.
    rbtree.foldl((pred(V::in, _::in, In::in, Out::out) is det :-
        (
            In = maybe.yes(_),
            Out = In
        ;
            In = maybe.no,
            ( if
                close_enough(Target, V)
            then
                Out = maybe.yes(V)
            else
                Out = In )
        )),
        !.Resistors, maybe.no, MaybeCloseEnough),
    (
        require_det (
            MaybeCloseEnough = maybe.yes(Value),
            Result = resistor(Value)
        )
    ;
        MaybeCloseEnough = maybe.no,
        
        % Test to see if we can make a series network or a parellel network
        % that equals this.
        (
            simple_series(rec_depth - 2, Target, [], Result, !Resistors)
        ;
            simple_parallel(rec_depth - 2, Target, [], Result, !Resistors)
        )
    ).

%-----------------------------------------------------------------------------%
% rec_series(MaxDepth,
%    Target,
%    InputNetwork,
%    OutNetwork,
%    !Resistors)
:- pred rec_series(int, int,
    list.list(network),
    network,
    resistor_set, resistor_set).
:- mode rec_series(in, in, in, out, in, out) is nondet.

rec_series(MaxDepth,
    Target,
    InputList,
    OutNetwork,
    !Resistors) :-
    MaxDepth > 0,
    InputNetwork = series(InputList),
    NetworkResistance = value(InputNetwork),
    ( if
        % If we found a result, just go with it.
        Target - tolerance(Target) =< NetworkResistance,
        Target + tolerance(Target) >= NetworkResistance
    then
        % Solved.
        OutNetwork = InputNetwork
    else
        (
            % Switch to a parallel network, if this network was not empty.
            InputList = [_|[_|_]],
            rec_parallel(MaxDepth,
                Target,
                [InputNetwork|[]],
                OutNetwork,
                !Resistors)
        ;
            % Add more to this existing network.
            rbtree.member(!.Resistors, Value, Count),
            ( if
                Count =< 1
            then
                rbtree.delete(Value, !Resistors)
            else
                rbtree.set(Value, Count - 1, !Resistors)
            ),
            rec_series(MaxDepth - 1,
                Target,
                [resistor(Value)|InputList],
                OutNetwork,
                !Resistors)
        )
    ).

%-----------------------------------------------------------------------------%
% rec_parallel(MaxDepth, Target,
%    InputNetwork, InputResistanceSum,
%    OutNetwork,
%    !Resistors)
:- pred rec_parallel(int, int,
    list.list(network),
    network,
    resistor_set, resistor_set).
:- mode rec_parallel(in, in, in, out, in, out) is nondet.

rec_parallel(MaxDepth,
    Target,
    InputList,
    OutNetwork,
    !Resistors) :-
    MaxDepth > 0,
    InputNetwork = parallel(InputList),
    NetworkResistance = value(InputNetwork),
    ( if
        % If we found a result, just go with it.
        InputList \= [],
        Target - tolerance(Target) =< NetworkResistance,
        Target + tolerance(Target) >= NetworkResistance
    then
        % Solved.
        OutNetwork = InputNetwork
    else
        (
            % Switch to a series network, if this network was not empty.
            InputList = [_|[_|_]],
            rec_series(MaxDepth,
                Target,
                [InputNetwork|[]],
                OutNetwork,
                !Resistors)
        ;
            % Add more to the existing network.
            rbtree.member(!.Resistors, Value, Count),
            ( if
                Count =< 1
            then
                rbtree.delete(Value, !Resistors)
            else
                rbtree.set(Value, Count - 1, !Resistors)
            ),
            rec_parallel(MaxDepth - 1,
                Target,
                [resistor(Value)|InputList],
                OutNetwork, !Resistors)
        )
    ).

%-----------------------------------------------------------------------------%
% rec(MaxDepth, Target, Result, !Resistors
:- pred rec(int, int, network, resistor_set, resistor_set).
:- mode rec(in, in, out, in, out) is nondet.

rec(MaxDepth, Target, Result, !Resistors) :-
    % See if there are any values which are close enough.
    rbtree.foldl((pred(V::in, _::in, In::in, Out::out) is det :-
        (
            In = maybe.yes(_),
            Out = In
        ;
            In = maybe.no,
            ( if
                close_enough(Target, V)
            then
                Out = maybe.yes(V)
            else
                Out = In )
        )),
        !.Resistors, maybe.no, MaybeCloseEnough),
    (
        require_det (
            MaybeCloseEnough = maybe.yes(Value),
            Result = resistor(Value)
        )
    ;
        MaybeCloseEnough = maybe.no,
        
        % Test to see if we can make a series network or a parellel network
        % that equals this.
        (
            rec_series(MaxDepth, Target, [], Result, !Resistors)
        ;
            rec_parallel(MaxDepth, Target, [], Result, !Resistors)
        )
    ).

%-----------------------------------------------------------------------------%

main(!IO) :-
    io.read_line_as_string(StringResult, !IO),
    (
        StringResult = io.ok(String),
        ( if
            string.to_int(string.strip(String), Target)
        then
            % Read from stdin
            read_resistors_from_stdin(rbtree.init, Resistors, !IO),
            Sort = (pred(A::in, B::in, Cmp::out) is det :-
                builtin.compare(LenCmp, length(A), length(B)),
                % If the lengths are identical, sort by how close the
                % resistance is to our target.
                ( if
                    LenCmp = (=)
                then
                    % Not entirely sure why the type hinting is needed...
                    builtin.compare(Cmp,
                        abs(value(A) - Target):int,
                        abs(value(B) - Target))
                else
                    LenCmp = Cmp
                ) ),
            
            % Try a simple network.
            promise_equivalent_solutions [SimpleResults] (
                solutions.unsorted_solutions((pred(Result::out) is nondet :-
                    simple(Target, Result, Resistors, _) ),
                    UnsortedSimpleResults),
                list.sort(Sort, UnsortedSimpleResults, SimpleResults)
            ),
            
            % We will also try a recursive network, limiting its max size to
            % the smallest simple network we found + 1.
            % The + 1 is a pretty sloppy heuristic, since (for me at least) you
            % can have certain "precious" resistors that you only have a few of
            % and it's sometimes worth it to use a few more very common values
            % in place of one. This gives us a better chance of seeing such
            % alternatives.
            ( if
                list.head(SimpleResults) = SemiShortest
            then
                RecMax = length(SemiShortest) + 1
            else
                RecMax = rec_depth
            ),
            
            promise_equivalent_solutions [RecResults] (
                solutions.unsorted_solutions((pred(Result::out) is nondet :-
                    rec(RecMax, Target, Result, Resistors, _) ),
                    UnsortedRecResults),
                list.sort(Sort, UnsortedRecResults, RecResults)
            ),
            
            list.merge(Sort, RecResults, SimpleResults, Results),
            
            list.foldl(print, Results, !IO)
        else
            io.write_string("Could not read target resistance of """, !IO),
            io.write_string(String, !IO),
            io.write_string("""\n", !IO)
        )
    ;
        StringResult = io.eof,
        io.write_string("Broken pipe.\n", !IO)
    ;
        StringResult = io.error(Error),
        io.write_string("IO Error: ", !IO),
        io.write_string(io.error_message(Error), !IO),
        io.nl(!IO)
    ).
